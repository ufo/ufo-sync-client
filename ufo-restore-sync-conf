#!/usr/bin/python
import os
import sys
import traceback
from optparse import OptionParser

usage = "usage: %prog [options] remote local"
parser = OptionParser(usage)
parser.add_option("-u", "--user", dest="user", default=os.environ["USER"],
                  help="login of the user", metavar="USER")
parser.add_option("-p", "--password", dest="password", default="",
                  help="Kerberos password", metavar="PASSWORD")
parser.add_option("-c", "--check", dest="check", default=False,
                  action="store_true", help="only check for something to restore")
parser.add_option("-m", "--mount", dest="mount", default=False,
                  action="store_true", help="try to mount filesystem")
parser.add_option("--no-conf", dest="config", default=True,
                  action="store_false", help="do not restore configurations")
parser.add_option("--no-apps", dest="apps", default=True,
                  action="store_false", help="do not restore applications")

(options, args) = parser.parse_args(args=sys.argv[1:])

if len(args) != 2:
    parser.error("Incorrect number of arguments")

fspath, home = args

if options.mount:
    os.system("mount %s &>/dev/null" % fspath)
    if os.system("mountpoint %s &>/dev/null" % fspath):
        print >> sys.stderr, fspath, "is not a mountpoint"
        sys.exit(1)

logged_in = os.system("/usr/kerberos/bin/klist &>/dev/null") == 0
if not logged_in:
    if options.password:
        logged_in = os.system("echo '%s' | /usr/kerberos/bin/kinit %s &>/dev/null" % (options.password, options.user))
    else:
        logged_in = os.system("/usr/kerberos/bin/kinit %s 2>/dev/null" % options.user) == 0

if logged_in and os.path.exists(os.path.join(fspath, options.user, ".confsync")):
    if options.check:
        sys.exit(0)

    if options.config:
        # Firstly copy configuration files from nfs to user home dir
        os.system("rsync -Oavr --exclude=rpmlist " + os.path.join(fspath, options.user, ".confsync/") + " " + home + " 2>&1 1>/dev/null")

    if options.apps:
        # Then saved package list
        list_pkg = ""
        if os.path.exists(os.path.join(fspath, options.user, ".confsync", "rpmlist")):
            list_pkg = open(os.path.join(fspath, options.user, ".confsync", "rpmlist")).read()

        os.system("umount " + fspath + " 2>&1 1>/dev/null")

        try:
            import yum
            yb = yum.YumBase()
            for l in list_pkg.splitlines():
                yb.install(name = l)
            yb.resolveDeps()
            yb.processTransaction()
        except:
            print traceback.format_exc()

    sys.exit(0)

sys.exit(2)
