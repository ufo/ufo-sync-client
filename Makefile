NAME=tsumufsmount
VERSION=0.2
ARCH=noarch
SOURCES=

all:
	
install:
	install -D -m 755 tsumufsmount $(DESTDIR)/usr/bin/tsumufsmount
	install -D -m 755 create-sync-symlink.desktop $(DESTDIR)/etc/xdg/autostart/create-sync-symlink.desktop
	install -D -m 755 create-sync-symlink $(DESTDIR)/usr/bin/create-sync-symlink
	install -D -m 755 ufo-conf-sync-loop.desktop $(DESTDIR)/etc/xdg/autostart/ufo-conf-sync-loop.desktop
	install -D -m 755 ufo-conf-sync-loop $(DESTDIR)/usr/bin/ufo-conf-sync-loop
	install -D -m 755 excludesync.list $(DESTDIR)/usr/share/tsumufsmount/excludesync.list
	install -D -m 755 01-tsumufs $(DESTDIR)/etc/NetworkManager/dispatcher.d/01-tsumufs
	install -D -m 755 tsumufs.modules $(DESTDIR)/etc/sysconfig/modules/tsumufs.modules
	install -D -m 755 ufo-conf-sync $(DESTDIR)/usr/bin/ufo-conf-sync
	install -D -m 755 ufo-restore-sync-conf $(DESTDIR)/usr/bin/ufo-restore-sync-conf
	install -D -m 644 tsumufs.conf $(DESTDIR)/etc/tsumufs/tsumufs.conf
